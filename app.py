from flask import Flask, render_template, request
from flask.ext.uploads import UploadSet, configure_uploads, IMAGES
import commands
import cv2

app = Flask(__name__)

photos = UploadSet('photos', IMAGES)

app.config['UPLOADED_PHOTOS_DEST'] = 'static/img'
configure_uploads(app, photos)


@app.route("/")
def main():
	return render_template('index.html')

@app.route("/toytask1", methods=['GET', 'POST'])
def toyTask1():
	print "toytask1"
	if request.method == 'POST' and 'photo' in request.files:
		filename = photos.save(request.files['photo'])
		return render_template('toytask1.html',imgsrc = '/static/img/'+filename,filename=filename) 
	if request.method == 'POST' and 'question' in request.form and 'filename' in request.form:
		question = request.form['question']
		filename = request.form['filename']
		cmd='th predict.lua -checkpoint_file checkpoints/vqa_epoch15.00_0.4576.t7 -input_image_path '+'static/img/'+filename+" -question '"+question+"' -gpuid 0"
		s=commands.getstatusoutput(cmd)
		print s[1]
		print cmd
		return render_template('toytask1.html',imgsrc = '/static/img/'+filename, question='Question: '+question, answer='Answer: '+s[1].split("\n")[-1]) 
	return render_template('toytask1.html',imgsrc = '/static/img/default.jpg')

@app.route("/toytask2", methods=['GET', 'POST'])
def toyTask2():
	print "toytask2"
	if request.method == 'POST' and 'photo' in request.files:
		filename = photos.save(request.files['photo'])
		return render_template('toytask2.html',imgsrc = '/static/img/'+filename,filename=filename,modules=['Gaussblur','Thresholding','RGB2Gray','Canny-Edge']) 
	if request.method == 'POST' and 'pipeline' in request.form and 'filename' in request.form:
		pipeline = request.form['pipeline']
		filename = request.form['filename']
		pipArray=pipeline.split(',')
		pipArray.pop()
		print pipArray
		urlstr=[['input','/static/img/'+filename]]
		for idx, mod in enumerate(pipArray):
			urlstr.append([mod,'/static/img/'+filename.split('.')[0]+mod+str(idx)+'.jpg'])
			print idx
			print urlstr[0] 
			print urlstr[idx]
			imageprocessing(mod,urlstr[idx][1],urlstr[idx+1][1])
		return render_template('toytask2.html',imgsrc = '/static/img/'+filename, entries=urlstr,modules=['Gaussblur','Thresholding','RGB2Gray','Canny-Edge'],message=pipArray) 
	return render_template('toytask2.html',imgsrc = '/static/img/default.jpg',modules=['Gaussblur','Thresholding','RGB2Gray','Canny-Edge'])


def imageprocessing(mod,inputPath,outputPath):
	print 'here'
	img = cv2.imread(inputPath[1:])
	if mod=='Gaussblur':
		blur = cv2.GaussianBlur(img,(5,5),0)
		cv2.imwrite(outputPath[1:], blur)
	elif mod=='Canny-Edge':
		edges = cv2.Canny(img,100,200) 
		cv2.imwrite(outputPath[1:], edges)
	elif mod=='RGB2Gray':
		gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		cv2.imwrite(outputPath[1:], gray_image)
	elif mod=='Thresholding':
		ret,thresh = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
		cv2.imwrite(outputPath[1:], thresh)
	return







if __name__ == "__main__":
    app.run(debug=True)
